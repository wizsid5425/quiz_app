from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied
from django.core.exceptions import ValidationError


class Attempt(models.Model):
    user = models.ForeignKey(User)

    def get_user(self):
        return self.user


class QuestionAttempt(models.Model):
    choices = (("WR", "WRONG"),
               ("CR", "CORRECT"),
               ("UA", "NOT ATTEMPTED"))
    question = models.CharField(max_length=200)
    ans = models.CharField(max_length=1)
    response = models.CharField(max_length=1, null=True)
    result = models.CharField(max_length=2, choices=choices, default="UA")
    attempt = models.ForeignKey(Attempt, related_name='question_attempts')

    def save(self, *args, **kwargs):
        if self.result is "UA":
            if self.response is not None:
                if self.response == self.ans:
                    self.result = "CR"
                else:
                    self.result = "WR"
                super(QuestionAttempt, self).save(*args, **kwargs)
        else:
            raise PermissionDenied(detail="You can only answer once. Sorry!")

    def get_user(self):
        return self.attempt.user