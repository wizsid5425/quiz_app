from rest_framework.routers import SimpleRouter
from .views import AttemptViewSet, QuestionAttemptViewSet

router = SimpleRouter()
router.register('attempts', AttemptViewSet)
router.register('question_attempt', QuestionAttemptViewSet)

urlpatterns = router.urls
