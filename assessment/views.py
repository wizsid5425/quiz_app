from rest_framework.viewsets import ModelViewSet, GenericViewSet
from .serializers import AttemptSerializer, QuestionAttemptSerializer
from .models import QuestionAttempt, Attempt
from .permissions import IsOwnerOnly
from rest_framework import mixins
from django.contrib.auth.models import User


class AttemptViewSet(GenericViewSet, mixins.RetrieveModelMixin):
    serializer_class = AttemptSerializer
    queryset = Attempt.objects.filter()
    permission_classes = (IsOwnerOnly,)


class QuestionAttemptViewSet(GenericViewSet, mixins.UpdateModelMixin):
    serializer_class = QuestionAttemptSerializer
    queryset = QuestionAttempt.objects.filter()
    permission_classes = (IsOwnerOnly,)