from rest_framework import serializers
from .models import Attempt, QuestionAttempt
from django.contrib.auth.models import User


class QuestionAttemptSerializer(serializers.ModelSerializer):
    response_status = serializers.SerializerMethodField()

    class Meta:
        model = QuestionAttempt
        fields = ("question", "id", "response", "response_status")
        extra_kwargs = {"response": {"write_only": True}}

    def get_response_status(self, obj):
        return obj.get_result_display()


class AttemptSerializer(serializers.ModelSerializer):
    question_attempts = QuestionAttemptSerializer(many=True)
    user = serializers.PrimaryKeyRelatedField(
        default=serializers.CurrentUserDefault(),
        read_only=True,
    )

    class Meta:
        model = Attempt
        fields = ("question_attempts", "user")